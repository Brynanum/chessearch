# Chessearch
![Illustration of the project](https://gitlab.com/Brynanum/chessearch/raw/master/chessearch.png)

## Introduction
Research module on chess games by information in NodeJS, MongoDB, JS, HTML and CSS.

## Documentation
### Dependancies
* NodeJS
* MongoDB
### How to install the project
* Install dependancies
* Create a MongoDB database nammed `chessearch`, with collections `Game` and `Position` (See the subpart "The database" below)
* Launch `app.js` with NodeJS, and access it on `localhost:3000`

## The database
This is the schema of the MongoDB database (`chessearch` by default) :

| *Game*   |     | *Position* |
|:--------:|:---:|:----------:|
| id       | <-- | game       |
| Event    |     | move       |
| Date     |     | fen        |
| White    |     |            |
| WhiteELO |     |            |
| Black    |     |            |
| BlackELO |     |            |
| Result   |     |            |
| ECO      |     |            |
| Moves    |     |            |

`Position` is an optional collection. However, that one permit to do researches on more criteria.
There are examples of database in JSON format ('databaseExample...' files).
You can easily create this database from JSON files by using this command :
`mongoimport --db=[dbName] --collection=[collectionName] --file=fileName.json --jsonArray`

## License
MIT

# Contributor
Bryan Fauquembergue

# Improvement
This project is finished. However, if you want to improve it, do not hesitate to fork it.
