var view=require('./view.js');
var controller=require('./controller.js');

const route=require('express').Router();
	// ==== Return the started view
	route.get('/',(req,res)=>{
		view.view(res);
	});
	
	// ==== Search results according to filters
	route.get('/api',(req,res)=>{
		controller.api(res,req.query);
	});

	// ==== Return the result of the request in HTML table
	route.get('/search',(req,res)=>{
		view.search(res,req.query);
	});
module.exports=route;
