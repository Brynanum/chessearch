const mongoose=require('mongoose');
	mongoose.connect('mongodb://localhost/chessearch',{useUnifiedTopology: true, useNewUrlParser: true});
	var db=mongoose.connection;
	db.on('error',console.error.bind(console,'connection error:'));
const bodyParser=require('body-parser');
const express=require('express');
	const app=express();
	app.use(bodyParser.urlencoded({extended: true}));
		const route=require('./route.js');
		app.use(route);
		app.listen(3000);

console.log('[chessearch] Listening on localhost on port 3000...');
