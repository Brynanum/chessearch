var controller=require('./controller.js');
var model=require('./model.js');

// ==== Return the view
module.exports.view = function(response) {
	html=`
		<!DOCTYPE html>
		<html>
			<head>
				<meta charset="utf-8" />
				<title>Chessearch</title>
				<style>
					section {
						margin-top: 20px;
					}
					section iframe {
						width: 100%;
						min-height: 500px;
					}
				</style>
			</head>
			<body>
				<header><form id="filters" onsubmit="return filtersToUrl();">
	`;
	// ---- Generate <input> -------------
	for(let field in model.Game.schema.obj) {
		html+='<input type="text" name="'+field+'" placeholder="'+field+'" />';
	}
	// -------------------------------------
	html+=`
					<input type="text" name="fen" placeholder="fen" />
					<input type="text" name="Player" placeholder="Player" />
					<input type="submit" />
				</form></header>
				<section>
					<iframe src=""></iframe>
				</section>
			</body>
			<script>
				function filtersToUrl() {
					let filters = document.getElementById('filters').querySelectorAll('input');
					let url="search?";
					for(let filter of filters) {
						if(filter.value!="")
							url+=filter.name+'='+filter.value+'&';
					}
					
					let iframe = document.querySelector('iframe');
					iframe.src=url;
										
					return false;
				}
			</script>
		</html>
	`;
	response.send(html);
}


// ==== Put elements in HTML table
module.exports.search = async function(response,filters) {
	function htmlArray(error,games){
		html=`
			<!DOCTYPE html>
			<html>
				<head>
					<meta charset="utf-8" />
					<title>Chessearch - Results</title>
					<style rel="stylesheet">
						table tr th {
							text-align: left;
						}
						table tr th, table tr td {
							border-left: 1px solid black;
						}
					</style>
				</head>
				<body><table>
					<thead><tr>
		`;
		// ---- Generate head <tr> -------------
		for(let field in model.Game.schema.obj) {
			html+='<th>'+field+'</th>';
		}
		// -------------------------------------
		html+=`
						<th>fen</th>
					</tr></thead>
					<tbody>
		`;
		// ---- Transform each game to <tr> ----
		for(let game of games) {
			html+='<tr>';
				if(typeof filters['fen'] === "undefined" || game.Positions.length==0) {
					game['Moves']=game['Moves'].split(',').join("<br />");
				} else if(game.Positions.length==1) {
					game['Moves']=game.Positions[0]['move'];
				}
				for(let field in model.Game.schema.obj) {
					html+='<td>'+game[field]+'</td>';
				}
				html+='<td>';
					if(game.Positions.length==0) {
						html+='? ? ? ? ? ?';
					} else { for(let position of game.Positions) {
						html+=position['fen']+'<br />';
					}}
				html=html.substr(0,html.length-1)+'</td>';
			html+='</tr>';
		}
		// -------------------------------------
		html+=`
					</tbody>
				</table></body>
			</html>
		`;
		response.send(html);
	}
	
	if(typeof filters['fen'] !== "undefined") {
		await model.Game.find(controller.filtersVerify(filters)).populate({path:'Positions',match:{fen:filters['fen']}}).exec(htmlArray);
	} else {
		await model.Game.find(controller.filtersVerify(filters)).populate('Positions').exec(htmlArray);
	}
}
