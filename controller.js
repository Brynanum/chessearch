var model=require('./model.js');

// ==== Treatment of filters
module.exports.filtersVerify=function(filters) {
	query={};
	if(typeof filters['Player'] !== "undefined") {
		query['$or'] = [
			{'White': filters['Player']},
			{'Black': filters['Player']}
		];
	}
	for(let filter of model.GameColumns) {
		if(typeof filters[filter] !== "undefined") {
			query[filter] = filters[filter];
		}
	}
	return query;
}

// ==== Search elements that match with filters
module.exports.api = async function(response,filters) {
	if(typeof filters['fen'] !== "undefined") {
		filterfen=filters['fen'];
		await model.Game.find(module.exports.filtersVerify(filters)).populate({path:'Positions',match:{fen:filterfen}}).exec(function(error,games){
			if(error){console.log(error);}
			response.json(games);
		});
	} else {
		await model.Game.find(module.exports.filtersVerify(filters)).populate('Positions').exec(function(error,games){
			response.json(games);
		});
	}
}
