const mongoose=require('mongoose');

// ==== Game
module.exports.GameColumns = [
	'id',
	'Event',
	'Date',
	'White',
	'WhiteELO',
	'Black',
	'BlackELO',
	'Result',
	'ECO',
	'Moves'
];
module.exports.GameSchema = new mongoose.Schema({
	id: Number,			// Identifiant
	Event: String,		// Event when this game appears
	Date: String,		// Date of the game
	White: String,		// Name of the white player
	WhiteELO: String,	// Rating ELO of the white player
	Black: String,		// Name of the black player
	BlackELO: String,	// Rating ELO of the black player
	Result: String,		// Result of the game
	ECO: String,		// Classification
	Moves: String,		// Game in sample PGN format
});

module.exports.GameSchema.virtual('Positions',{
	ref: 'Position',
	localField: 'id',
	foreignField: 'game',
});
module.exports.GameSchema.set('toObject', { virtuals: true });
module.exports.GameSchema.set('toJSON', { virtuals: true });
module.exports.Game = mongoose.model('Game',module.exports.GameSchema,'Game');

// ==== Position
module.exports.PositionColumns = [
	'fen',
	'move',
	'game'
];
module.exports.PositionSchema=new mongoose.Schema({
	fen: String,		// Forsyth–Edwards Notation (representation of the game)
	move: String,		// The associated move with the FEN
	game: Number,		// Identifiant of the specific game
});
module.exports.PositionSchema.virtual('Game',{
	ref: 'Game',
	localField: 'game',
	foreignField: 'id',
});
module.exports.PositionSchema.set('toObject', { virtuals: true });
module.exports.PositionSchema.set('toJSON', { virtuals: true });
module.exports.Position = mongoose.model('Position',module.exports.PositionSchema,'Position');
